"""pepew URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from pages.views import *

urlpatterns = [
    path('home/', home_view, name='home'),
    path('', home2_view, name='home2'),
    path('portfolio/', portfolio_view, name="portfolio"),
    path('programming/', programming_view, name="programming"),
    path('photography/', photography_view, name="photography"),
    path('profile/', profile_view, name="profile"),
    path('matkulCreate/', matkul_form_create, name="matkul_form_create"),
    path('matkulList/', matkul_list_view, name="matkul_list_view"),
    path('matkulDelete/<str:pk>/', matkul_delete_view, name="matkul_delete"),
    path('matkulDetail/<str:pk>/', matkul_detail_view, name="matkul_detail_view"),
    path('kegiatanList/', kegiatan_list_view, name="kegiatan_list_view"),
    path('kegiatanAdd/', kegiatan_add_view, name="kegiatan_add_view"),
    path('kegiatanDelete/<str:pk>/', kegiatan_delete_view, name="kegiatan_delete_view"),
    path('orangDelete/<str:pk>/', orang_delete_view, name="orang_delete_view"),
    path('booksSearch/', books_search, name="books_search"),
    path('login/', login_view, name="login"),
    path('signup/', signup_view, name="signup"),
    path('logout/', logout_view, name="logout"),
    path('testpage/', testpage_view, name="testpage"),
    path('admin/', admin.site.urls),
]
