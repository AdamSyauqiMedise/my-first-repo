from django.contrib.auth.models import User
from django.http import response
from django.test import TestCase, Client
from .models import *

# Create your tests here.
class StoryLima(TestCase):

    def setUp(self):
        namaMatkul = "PPW"
        dosenPengajar = "Pak Adin"
        jumlahSKS = 3
        deskripsi = "Kuliah Bikin Website"
        semester = "GSL2021"
        ruangKelas = "PJJ"
        Matkul.objects.create(namaMatkul=namaMatkul, dosenPengajar=dosenPengajar, jumlahSKS=jumlahSKS, deskripsi=deskripsi, semester=semester, ruangKelas=ruangKelas)

    def test_matkul_form_create(self):
        response = Client().get('/matkulCreate/')
        self.assertTemplateUsed(response, 'matkul_create_form.html')
        self.assertEqual(response.status_code, 200)
        cnt = Matkul.objects.all().count()
        posting = Client().post('/matkulCreate/', data={'namaMatkul':'SDA', 'dosenPengajar':'Bu Naya', 'jumlahSKS':3, 'deskripsi':'DDP3', 'semester':'GSL2021', 'ruangKelas':'PJJ'})
        self.assertEqual(Matkul.objects.all().count(), cnt+1)
        self.assertEqual(posting.status_code, 200)
    
    def test_matkul_list_view(self):
        response = Client().get('/matkulList/')
        self.assertTemplateUsed(response, 'matkul_list.html')
        self.assertEqual(response.status_code, 200)
    
    def test_matkul_delete_view(self):
        response = Client().get('/matkulDelete/' + str(1) +'/')
        self.assertTemplateUsed(response, 'matkul_delete.html')
        POK = Matkul.objects.create(namaMatkul="POK", dosenPengajar="Bu Efi", jumlahSKS=3, deskripsi="Lanjutan PSD", semester="GSL2021", ruangKelas="PJJ")
        cnt = Matkul.objects.all().count()
        self.assertEqual(cnt, 2)
        posting = Client().post('/matkulDelete/' + str(POK.id) + '/')
        cnt = Matkul.objects.all().count()
        self.assertEqual(cnt, 1)
        self.assertEqual(posting.status_code, 302)

    def test_matkul_detail_view(self):
        response = Client().get('/matkulDetail/' + str(1) +'/')
        self.assertTemplateUsed(response, 'matkul_detail.html')   

class StoryEnam(TestCase):

    def setUp(self):
        Kegiatan.objects.create(namaKegiatan = "Simping")
        Kegiatan.objects.create(namaKegiatan = "Genshin")
        Kegiatan.objects.create(namaKegiatan = "Cosplay")
        Kegiatan.objects.create(namaKegiatan = "Arknek")

    def test_model_kegiatan(self):
        Simping = Kegiatan.objects.get(namaKegiatan = "Simping")
        self.assertEqual(str(Simping), "Simping")
    
    def test_model_orang(self):
        Pancen = Orang.objects.create(namaOrang = "Pancen", kegiatanOrang=Kegiatan.objects.get(namaKegiatan="Genshin"))
        self.assertEqual(str(Pancen), "Pancen")
        self.assertEqual(str(Pancen.kegiatanOrang), "Genshin")
    
    def test_kegiatan_list_view(self):
        response = Client().get('/kegiatanList/')
        self.assertTemplateUsed(response, 'kegiatan_list.html')
        self.assertEqual(response.status_code, 200)
        cnt = Orang.objects.all().count()
        posting = Client().post('/kegiatanList/', data={'namaOrang':'Sena','kegiatanOrang':'1'})
        self.assertEqual(Orang.objects.all().count(), cnt+1)
        self.assertEqual(posting.status_code, 302)
        
    def test_kegiatan_add_view(self):
        response = Client().get('/kegiatanAdd/')
        self.assertTemplateUsed(response, 'kegiatan_add.html')
        self.assertEqual(response.status_code, 200)
        cnt = Kegiatan.objects.all().count()
        posting = Client().post('/kegiatanAdd/', data={'namaKegiatan':'Arknights'})
        self.assertEqual(Kegiatan.objects.all().count(), cnt+1)
        self.assertEqual(posting.status_code, 302)

    def test_kegiatan_delete_view(self):
        Cosplay = Kegiatan.objects.create(namaKegiatan="Cosplay")
        cnt = Kegiatan.objects.all().count()
        self.assertEqual(cnt, 5)
        response = Client().post('/kegiatanDelete/' + str(Cosplay.id) + '/')
        cnt = Kegiatan.objects.all().count()
        self.assertEqual(cnt, 4)

    def test_orang_delete_view(self):
        Gacha = Kegiatan.objects.create(namaKegiatan="Gacha")
        Adit = Orang.objects.create(namaOrang = "Adit", kegiatanOrang=Kegiatan.objects.get(namaKegiatan="Gacha"))
        cnt = Orang.objects.all().count()
        self.assertEqual(cnt, 1)
        response = Client().post('/orangDelete/' + str(Adit.id) + '/')
        cnt = Orang.objects.all().count()
        self.assertEqual(cnt, 0)

class StoryDelapan(TestCase):
    
    def setUp(self):
        self.credentials = {
            'username': 'Larrissa_Nee_Chan',
            'password': 'Goddess'
            }
        User.objects.create_user(**self.credentials)

    def test_books_search(self):
        logging_in = self.client.post('/login/', self.credentials, follow=True)
        response = self.client.get('/booksSearch/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'books_search.html')

class StorySembilan(TestCase):

    def test_signup_page(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

        self.credentials = {
            'username': 'Larissa', 
            'email': 'larissa@gmail.com', 
            'password1': 'Kami-sama', 
            'password2': 'Kami-sama'
            }

        signing_up = self.client.post('/signup/', data={'username': 'Larissa', 'email': 'larissa@gmail.com', 'password1': 'Kami-sama', 'password2': 'Kami-sama'})
        self.assertEqual(signing_up.status_code, 302)
        loggin_in = self.client.post('/login/', data={'username': 'Larissa', 'password': 'Kami-sama'})
        self.assertEqual(loggin_in.status_code, 302)        
