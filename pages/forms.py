from django import forms
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

#iterable
CHOICES = (
    ("GSL1920", "Gasal 2019/2020"),
    ("GNP1920", "Genap 2019/2020"),
    ("GSL2021", "Gasal 2020/2021"),
    ("GNP2021", "Genap 2020/2021"),
    ("GSL2122", "Gasal 2021/2022"),
    ("GNP2122", "Genap 2021/2022"),
    ("GSL2223", "Gasal 2022/2023"),
    ("GNP2223", "Genap 2022/2023"),
)

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'namaMatkul',
            'dosenPengajar',
            'jumlahSKS',
            'deskripsi',
            'semester',
        ]
        labels = {
            'namaMatkul' : 'Course Name',
            'dosenPengajar' : 'Lecturer',
            'jumlahSKS' : 'Credit Score',
            'deskripsi' : 'Description',
            'semester' : 'Term',
        }

class RawMatkulForm(forms.Form):
    namaMatkul = forms.CharField(label = 'Course Name', widget=forms.TextInput(attrs={"placeholder": "Your Course Name", "class": "input"}))
    dosenPengajar = forms.CharField(label = "Lecturer's Name", widget=forms.TextInput(attrs={"placeholder": "Your Lecturer's Name", "class": "input"}))
    jumlahSKS = forms.IntegerField(min_value=1, max_value=24, label = 'Credit Score', widget=forms.NumberInput(attrs={"placeholder": "1-24", "class": "input"}))
    deskripsi = forms.CharField(label = 'Description', widget=forms.Textarea(attrs={"placeholder": "Your Amazing Description", "class": "input"}))
    semester = forms.ChoiceField(choices = CHOICES)
    ruangKelas = forms.CharField(label = 'Course Room', widget=forms.TextInput(attrs={"placeholder": "Your Course Location", "class": "input"}))

class KegiatanForm(forms.Form):
    namaKegiatan = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Your Activity Name", "class": "input"}))

class OrangKegiatanForm(forms.Form):
    namaOrang = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Your Name", "class": "input"}))
    kegiatanOrang = forms.ModelChoiceField(queryset = Kegiatan.objects.all())

class CreateNewUserForm(UserCreationForm):
    password1 = forms.CharField(label=("Password"), widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'Password...'}))
    password2 = forms.CharField(label=("Password confirmation"), widget=forms.PasswordInput(attrs={'class': 'input', 'placeholder': 'Password Confirmation...'}))
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        widgets = {
            'username': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Username...'}),
            'email': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Email...'}),
        }
