from django.db import models
from django.core.validators import *

#CHOICES
CHOICES = (
    ("GSL1920", "Gasal 2019/2020"),
    ("GNP1920", "Genap 2019/2020"),
    ("GSL2021", "Gasal 2020/2021"),
    ("GNP2021", "Genap 2020/2021"),
    ("GSL2122", "Gasal 2021/2022"),
    ("GNP2122", "Genap 2021/2022"),
    ("GSL2223", "Gasal 2022/2023"),
    ("GNP2223", "Genap 2022/2023"),
)
# Create your models here.
class Matkul(models.Model):

    namaMatkul = models.CharField(max_length = 100)
    dosenPengajar = models.CharField(max_length = 100)
    jumlahSKS = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(24)])
    deskripsi = models.CharField(max_length = 1000)
    semester = models.CharField(max_length = 10, choices=CHOICES)
    ruangKelas = models.CharField(max_length = 100)
    #semester = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(8)])
    
    def __str__(self):
        return self.namaMatkul

class Kegiatan(models.Model):
    
    namaKegiatan = models.CharField(max_length = 100)

    def __str__(self):
        return self.namaKegiatan

class Orang(models.Model):

    kegiatanOrang = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, null=True)
    namaOrang = models.CharField(max_length = 100)

    def __str__(self):
        return self.namaOrang
