from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.forms import inlineformset_factory
from .forms import *
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.
def home_view(request):
    return render(request, 'index.html')

def home2_view(request):
    return render(request, 'home.html')

def portfolio_view(request):
    return render(request, 'portfolio.html')

def programming_view(request):
    return render(request, 'programming.html')

def profile_view(request):
    return render(request, 'profile.html')

def photography_view(request):
    return render(request, 'photography.html')

def matkul_form_create(request):
    form = RawMatkulForm()
    if request.method == "POST":
        form = RawMatkulForm(request.POST)
        if form.is_valid():
            Matkul.objects.create(**form.cleaned_data)
            form = RawMatkulForm()
    context = {
        'form' : form
    }
    return render(request, 'matkul_create_form.html', context)

def matkul_list_view(request):
    querySet = Matkul.objects.all()
    context = {
        "object_list" : querySet
    }
    return render(request, 'matkul_list.html', context)

def matkul_delete_view(request, pk):
    matkulInput = Matkul.objects.get(id=pk)
    if request.method == "POST":
        matkulInput.delete()
        return redirect('/matkulList')
    context = {
        "item" : matkulInput
    }
    return render(request, 'matkul_delete.html', context)

def matkul_detail_view(request, pk):
    matkulDetail = Matkul.objects.get(id=pk)
    context = {
        "matkul_detail": matkulDetail
    }
    return render(request, 'matkul_detail.html', context)

def kegiatan_list_view(request):
    if request.method == "POST":
        form_orang = OrangKegiatanForm(request.POST)
        print(request.POST)
        if form_orang.is_valid():
            Orang.objects.create(**form_orang.cleaned_data)
            return redirect('/kegiatanList')

    kegiatanList = Kegiatan.objects.all()
    form_orang = OrangKegiatanForm()
    context = {
        "kegiatan_list" : kegiatanList,
        "form_orang" : form_orang
    }
    return render(request, 'kegiatan_list.html', context)

def kegiatan_add_view(request):
    form_kegiatan = KegiatanForm()
    if request.method == "POST":
        form_kegiatan = KegiatanForm(request.POST)
        if form_kegiatan.is_valid():
            Kegiatan.objects.create(**form_kegiatan.cleaned_data)
            return redirect('/kegiatanList')
    context = {
        'form_kegiatan' : form_kegiatan
    }
    return render(request, 'kegiatan_add.html', context)

def kegiatan_delete_view(request, pk):
    kegiatanInput = Kegiatan.objects.get(id=pk)
    if request.method == "POST":
        kegiatanInput.delete()
        return redirect('/kegiatanList')

def orang_delete_view(request, pk):
    orangInput = Orang.objects.get(id=pk)
    if request.method == "POST":
        orangInput.delete()
        return redirect('/kegiatanList')

@login_required(login_url='login')
def books_search(request):
    return render(request, 'books_search.html')

def login_view(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('/')
            else:
                messages.info(request, 'Username or Password is incorrect')
        context = {}
        return render(request, 'login.html', context)

def signup_view(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        form = CreateNewUserForm()

        if request.method == 'POST':
            form = CreateNewUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account by ' + user + ' was successfully created')

                return redirect('login')


        context = {'form': form}
        return render(request, 'signup.html', context)

def logout_view(request):
    logout(request)
    return redirect('login')

def testpage_view(request):
    return render(request, 'testpage.html')