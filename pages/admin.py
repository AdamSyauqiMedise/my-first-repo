from django.contrib import admin

from .models import *

admin.site.register(Matkul)
admin.site.register(Kegiatan)
admin.site.register(Orang)
# Register your models here.
