$(document).ready(function () {
    $('#accordion').accordion({
        collapsible: true,
        active: false,
        height: 'fill',
        header: 'h3'
    }).sortable({
        items: '.s_panel'
    });

    $('#accordion').on('accordionactivate', function (event, ui) {
        if (ui.newPanel.length) {
            $('#accordion').sortable('disable');
        } else {
            $('#accordion').sortable('enable');
        }
    });

    $("#button").click(function() {
        //console.log("Tets");
        var keyword = $("#keyword_buku").val();
        //console.log(keyword);
        var url_book = "https://www.googleapis.com/books/v1/volumes?q=" + keyword;
        //console.log(url_book);
        $.ajax({
            url: url_book,
            success: function(hasil) {
                //console.log(hasil.items);
                var book_container = $(".book_box_container");
                book_container.empty();
                if(hasil.items == null) {
                    book_container.append("<div class='is-size-3 has-text-centered'> Sorry! We weren't able to find the book you're looking for! </div>")
                } else {
                    for (i = 0; i < hasil.items.length; i++) {
                        var book_title = hasil.items[i].volumeInfo.title;
                        //console.log(book_title);
                        var book_description = hasil.items[i].volumeInfo.description;
                        //console.log(book_description);
                        var book_author = hasil.items[i].volumeInfo.authors;
                        //console.log(book_author);
                        var book_publisher = hasil.items[i].volumeInfo.publisher;
                        //console.log(book_publisher);
                        try {
                            var book_thumbnail = hasil.items[i].volumeInfo.imageLinks.thumbnail
                        } catch (TypeError) {
                            var book_thumbnail = "https://media.giphy.com/media/3zhxq2ttgN6rEw8SDx/giphy.gif";
                        }
                        
                        //if (hasil.items[i].volumeInfo.imageLinks.thumbnail) {
                            
                        //    console.log(book_thumbnail);
                        //} else {
                            
                        //    console.log(book_thumbnail);
                        //}
                        
                        //console.log(book_thumbnail);
    
                        book_container.append('<div class="box"> <article class="media"> <div class="media-left"> <figure class="img_class"> <img src= ' + book_thumbnail + ' > </figure> </div> <div class="media-content"> <div class="content"> <p> <strong> ' + book_title + '</strong> <small>' + book_author + '</small> <br>' + book_description + '</p> </div> </div> </article> </div>');
                    }
                }
            }
        });
    });
});

